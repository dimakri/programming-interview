class Test:
    # def __init__(self):
    #     self.a = a
    #     self.b = b
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def sum(self):
        return self.a + self.b


t = Test(2,3)

print(t.sum())

class TestChild(Test):
    def sum(self):
        return self.a * self.b

tc = TestChild(2,3)
print(tc.sum())