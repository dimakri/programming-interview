class Singleton:
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance

s = Singleton()
# print('1', s)
# s1 = Singleton()
# print('2', s1)
# print(dir(s1))
print(dir(Singleton))