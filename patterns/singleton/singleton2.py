class Singleton():
    _instances = {}

    def __call__(cls, *arg, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*arg, **kwargs)
        return cls._instances[cls]


class ConcreteClass(Singleton):
    pass


class ConcreteSubclass(Singleton):
    pass


# a = Singleton()
# b = Singleton()
# print(id(a)==id(b))
# print(Singleton())
print(ConcreteClass())
print(ConcreteSubclass())

# print(Singleton()==ConcreteClass())
