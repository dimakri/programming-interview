class Borg:
    _state = {}

    def __new__(cls, *arg, **kwargs):
        ob = super().__new__(cls, *arg, **kwargs)
        ob.__dict__ = cls._state
        return ob


a = Borg()
b = Borg()

class A(Borg):
    pass

class B(Borg):
    pass

print(A())
print(B())

### MODULE IS A SINGLETON!! ###