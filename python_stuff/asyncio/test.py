import asyncio

arr = []

async def func1(value):
    # print(1)
    print('started working with value {}'.format(value))
    await asyncio.sleep(1)
    arr.append(value*2)



    


ioloop = asyncio.get_event_loop()
# task = [ioloop.create_task(func1()), ioloop.create_task(func2()), ioloop.create_task(func3())]
task = []
for a in range(1,9):
    task.append(ioloop.create_task(func1(a)))
print(task)
wait_task = asyncio.wait(task)
ioloop.run_until_complete(wait_task)
ioloop.close()
print(arr)