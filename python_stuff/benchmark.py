import math

def f1(degrees):
    return math.cos(degrees)

def f2(degrees):
    e = 2.718281828459045
    return (e**(degrees *1j)).real

assert f1(100) == f2(100) == 12
assert f2(100)

import time
import random
import statistics

functions = f1, f2
times {f.__name__:[] for f in functions}