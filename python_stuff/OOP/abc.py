##
## http://www.daveoncode.com/2014/10/07/abstract-classes-in-python-using-abc-module/
##
from abc import ABC, abstractmethod

class AbstractClassExample(ABC):

    def __init__(self, value):
        self.value = value
        super().__init__()

    @abstractmethod
    def do_something(self):
        pass


class GetReturnValue(AbstractClassExample):
    def do_something(self):
        return self.value


class GetRetunrValueAdd(AbstractClassExample):
    def do_something(self):
        return self.value + 191


x = GetReturnValue(5)
print(x.do_something())

y = GetRetunrValueAdd(5)
print(y.do_something())