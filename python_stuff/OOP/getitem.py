# class Building:
#     def __init__(self, floors):
#         self.floors = [None]*floors
#     def occupy(self, floor_number, data):
#         self.floot[floor_number] = data
#     def get_floor_data(self, floor_number):
#         return self.floors[floor_number]


class Building:

    def __init__(self, floors):
        self.floors = [None]*floors

    def __setitem__(self, floor_number, data):
        self.floors[floor_number] = data

    def __getitem__(self, floor_number):
        return self.floors[floor_number]

    def __delitem__(self, floor_number):
        print('this is __delitem__ method')
        del self.floors[floor_number]


building = Building(4)
building[0] = 'Reception'
building[1] = 'IT Department'

print(building[1])
del building[0]
print(building[1])
