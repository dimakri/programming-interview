from collections import namedtuple

Animal = namedtuple('Animal', 'name age type')
perry = Animal(name='Perry', age = 13, type='cat')

print(perry)
print(perry.name)

print(perry[2])

print(perry._asdict())

perry.age = 2
# Traceback (most recent call last):
#   File "namedtuple.py", line 8, in <module>
#     perry.age = 2
# AttributeError: can't set attribute