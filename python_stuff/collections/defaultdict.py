from collections import defaultdict

colors = (
    ('Dima', 'Black'),
    ('Oleg', 'Pink'),
    ('Roma', 'Grey'),
    ('Andrey', 'White'),
    ('Eugine', 'White'),
    ('Eugine', 'Green'),
)

favorite_colors = defaultdict(list)

for name, color in colors:
    favorite_colors[name].append(color)

print(favorite_colors)
print(favorite_colors.keys())
print(favorite_colors.values())
print(favorite_colors.items())


string = 'hey hey hey yo yo, it is me, Dima !'

words = string.split()
print(words)

wordlocations = defaultdict(list)

for n, w in enumerate(words):
    wordlocations[w].append(n)

print(wordlocations)

import json
print(json.dumps(wordlocations))
