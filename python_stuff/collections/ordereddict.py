from collections import OrderedDict

# old way
color = {"Red": 198, "Blue": 170, "Green":160}
for key, value in color.items():
    print(key, value)



color = OrderedDict([("Red", 198),("Blue", 170),("Green", 160)])
for key, value in color.items():
    print(key,value)
