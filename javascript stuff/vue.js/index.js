var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!'
    }
});

var app2 = new Vue({
    el: "#app-2",
    data: {
        message: 'You loaded this page on ' + new Date().toLocaleTimeString(),
    }
});

var app3 = new Vue({
    el: '#app-3',
    data: {
        seen: true
    }
});

var app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [{
                text: 'Go for it!'
            },
            {
                text: 'Get after it!'
            },
            {
                text: 'Be strong and stay strong!'
            }
        ]
    }
});

var app5 = new Vue({
    el: '#app-5',
    data: {
        message: 'This is the initial message!!!'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('');
        }
    }
});

var app6 = new Vue({
    el: '#app-6',
    data: {
        message: 'Vue form!!!'
    }
});

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
});

var app7 = new Vue({
    el: '#app-7',
    data: {
        groceryList: [{
                id: 0,
                text: 'Steak'
            },
            {
                id: 1,
                text: 'Brocolli'
            },
            {
                id: 2,
                text: 'Coffe'
            }
        ]
    }
});

var app8 = new Vue({
    el: '#app-8',
    data: {
        color: 'blue'
    }
})



//   axios.get('http://127.0.0.1:8000/api/authors/',{headers: {
//     'Access-Control-Allow-Origin': '*'
//   }})
//   .then(function (response) {
//     console.log(response);
//   })
//   .catch(function (error) {
//     console.log(error);
//   });

var app9 = new Vue({
    el: '#app-9',
    data() {
        return {
            authorlist: 111
        }
    },
    mounted() {
        axios
            .get('http://127.0.0.1:8000/api/authors/')
            .then(response => (this.authorlist = response.data))
    }
});

// var app10 = new Vue({
//     el: '#app-10',
//     data
// })

// var getAuthor = function () {
//     axios.get('http://127.0.0.1:8000/api/authors/')
//         .then(function (response) {
//             // this.authorlist = response.data;
//             this.authorlist = [{
//                     text: 'Go for it!'
//                 },
//                 {
//                     text: 'Get after it!'
//                 },
//                 {
//                     text: 'Be strong and stay strong!'
//                 }
//             ];
//             console.log(JSON.stringify(response.data));
//             // console.log(this.authorlist)
//             // return this.authorlist;
//         })
//         .catch(function (error) {
//             console.log(error);
//         });
// }