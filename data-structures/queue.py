class Queue():
    def __init__(self):
        self.queue = []

    def enqueue(self, data):
        self.queue.insert(0, data)

    def size(self):
        return(len(self.queue))

    def dequeue(self):
        if self.is_empty():
            return None
        self.queue.pop()

    def is_empty(self):
        return len(self.queue) == 0


# qu = Queue()
# # qu.insert(1,1)
# # qu.insert(0,9)
# # qu.insert(2,99)
# qu.enqueue(1)
# qu.enqueue(99)
# qu.enqueue(43)
# qu.enqueue(12)
# print(qu.is_empty())
