class Node:

    def __init__(self, initdata):
        self.data = initdata
        self.next = None

    def getData(self):
        return self.data

    def getNext(self):
        return self.next

    def setData(self, newdata):
        self.data = newdata

    def setNext(self, newnext):
        self.next = newnext


class LinkedList():
    def __init__(self, head=None):
        self.head = head

    def insert(self, data):
        new_node = Node(data)
        new_node.setNext(self.head)
        self.head = new_node

    def delete_duplicates(self):
        current = self.head
        current2 = self.head
        while current:
            if current != None:
                current_data = current.getData().getData()
            while current2:
                if current2 != None:
                    # print (1000)
                    if current2.getData().getData() == current_data:
                        self.delete(current2.getData().getData())
                    current2 = current2.getNext()
                # print(current.getNext().getData())
                current = current.getNext()

    def has_duplicate(self, data):
        current = self.head
        count = 0
        while current:
            if current != None:
                if current.getData().getData() == data:
                    count += 1
                # print(current.getNext().getData())
                current = current.getNext()
        return count > 1
    
    def get_first(self):
        return self.head

    def size(self):
        current = self.head
        count = 0
        while current:
            count += 1
            current = current.getNext()
        return count

    # def search(self, data):
    #     current = self.head
    #     found = False
    #     while current and found is False:
    #         if current.getData().getData() == data:
    #             found = True
    #         else:
    #             current = current.getNext()
    #     if current is None:
    #         raise ValueError('Data is not on the list')
    #     return current

    def search(self, data):
        current = self.head
        # found = False
        while current:
            if current.getData().getData() == data:
                # print(current.getData().getData())
                # found = True
                break
            else:
                current = current.getNext()
        else:
            return -1
        # if current is None:
        #     raise ValueError('Data is not on the list')
        return current

    def traverse(self):
        current = self.head
        # print(1)
        while current:
            if current != None:
                print(current.getData())
                # print(current.getNext().getData())
                current = current.getNext()
            else:
                print('HZ')

    def delete(self, data):
        current = self.head
        previous = None
        found = False
        while current and found is False:
            if current.getData().getData() == data:
                found = True
            else:
                previous = current
                current = current.getNext()
        if current is None:
            raise ValueError('Data is not on the list')
        if previous is None:
            self.head = current.getNext()
        else:
            previous.setNext(current.getNext())

    def is_empty(self):
        return self.head == None

    def get_head(self):
        return self.head

    # def get_kth_from_last(self, k):
    #     current = self.head
    #     step = 0
    #     end = self.size() - k
    #     print('Should end at {}'.format(end))
    #     while current and step != end:
    #         print('Step:{} Data: {}'.format(step, current.getData()))
    #         step += 1
    #         current=current.getNext()
    #     return current

    def get_kth_from_last(self, k):
        current = self.head
        step = 0
        end = self.size() - k
        print('Should end at {}'.format(end))
        while current and step != k-1:
            print('Step:{} Data: {}'.format(step, current.getData()))
            step += 1
            current = current.getNext()
        return current

    def delete_middle(self):
        current = self.head
        previous = None
        step = 0
        middle = self.size() // 2
        while current and not step > middle:
            step += 1
            previous = current
            current = current.getNext()
            if step == middle:
                previous.setNext(current.getNext())

def add_values_in_list(self, list1, list2):
    pass

# def isPalindrom(self):
#     current = self.head
#     current2 = self.head
#     start_val = None
#     end_val = None
#     while current:
#          while current2:
#              if current2.getNext() = None
#              current2 = current.getData()



n1 = Node(2)
n2 = Node(5)
n3 = Node(10)
# n4 = Node(5)


new_list = LinkedList()
for i in range(15):
    new_list.insert(i)


# print(new_list.size())
print('Head: {}'.format(new_list.get_head().getData()))
# print(new_list.get_kth_from_last(3).getData())
print(new_list.size())
new_list.delete_middle()
print(new_list.size())
# print(new_list.traverse())
# new_list.insert(n2)
# new_list.insert(n3)
# new_list.insert(n4)


# print(n1.getNext().getData())
# print(new_list.traverse())
# print(new_list.delete_duplicates())

# print(new_list.get_first().getData().getData())
# print(new_list.size())
# print(new_list.search(11).getData().getData())
# new_list.delete(10)


# print(new_list.traverse())
# print(new_list.search(10).getData().getData())
# print(new_list.delete_duplicates())
# print(new_list.traverse())
# print(new_list.is_empty())

# print(new_list.get_head().getData().getData()) #why 10!?!?!?!?

+ 617
  295
  ---
  912