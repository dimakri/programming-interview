class BinaryTreeNode:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def add_left(self, left):
        self.left = left

    def add_right(self, right):
        self.right = right

    def __str__(self):
        text = str(self.data)
        text += ':'

        child = {}
        if self.left:
            child['left'] = str(self.left)
        if self.right:
            child['right'] = str(self.right)

        text += str(child)
        return text

root = BinaryTreeNode(4)

child_1 = BinaryTreeNode(3)
child_2 = BinaryTreeNode(2)

child_child_1 = BinaryTreeNode(44)
child_child_2 = BinaryTreeNode(63)
child_child_3 = BinaryTreeNode(23)
child_child_4 = BinaryTreeNode(64)

child_1.add_left(child_child_1)
child_1.add_right(child_child_2)
child_2.add_left(child_child_3)
child_2.add_right(child_child_4)

root.add_left(child_1)
root.add_right(child_2)

print(root)