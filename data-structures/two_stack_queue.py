class TwoStackQueue:
    def __init__(self):
        self.inbox = []
        self.outbox = []

    # inbox<<<<<----<<<<<---
    # outbox<<<<-----<<<<<<<

    def enqueue(self, val):
        if self.inboxIsEmpty:
            while not self.outboxIsEmpty():
                self.inbox.append(self.outbox.pop())
            self.inbox.append(val)
        else:
            self.inbox.append(val)

    def dequeue(self):
        if self.outboxIsEmpty:
            while not self.inboxIsEmpty():
                self.outbox.append(self.inbox.pop())
            self.outbox.pop()
        else:
            self.inbox.pop()

    def inboxIsEmpty(self):
        return len(self.inbox) == 0

    def outboxIsEmpty(self):
        return len(self.outbox) == 0

    def inboxSize(self):
        return len(self.inbox)

    def outboxSize(self):
        return len(self.outbox)


# a = TwoStackQueue()
# for i in range(9999):
#     a.enqueue(i)

# print(a.inboxSize())
# print(a.outboxSize())
# a.dequeue()
# a.dequeue()
# a.dequeue()
# a.enqueue(4)
# # a.dequeue()
# print(a.inboxSize())
# print(a.outboxSize())
# a.dequeue()
# print(a.inboxSize())
# print(a.outboxSize())
# a.enqueue(4)
# print(a.inboxSize())
# print(a.outboxSize())
# a.dequeue()
# print(a.inboxSize())
# print(a.outboxSize())
