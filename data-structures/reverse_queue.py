# Implement the Queue ADT, using a list such that the
# rear of the queue is at the end of the list.

class ReverseQueue:
    def __init__(self):
        self.item = []

    def enqueue(self, value):
        self.item.append(value)

    def dequeue(self):
        if self.isEmpty():
            return None
        # self.item.pop(0)
        self.item.reverse()
        self.item.pop()
        # self.item = i
         
        # del self.item[0]

    def size(self):
        return len(self.item)

    def isEmpty(self):
        return self.size() == 0


# a = ReverseQueue()
# print(a.size())
# print(a.isEmpty())
# a.enqueue(1)
# a.enqueue(2)
# a.enqueue(3)
# a.enqueue(4)

# print(a.size())
# print(a.isEmpty())

# a.dequeue()
# print(a.size())
# print(a)

