# Design and implement an experiment to do benchmark comparisons 
# of the two queue implementations. What can you learn from such an experiment?
from queue import Queue
from reverse_queue import ReverseQueue
from two_stack_queue import TwoStackQueue
import time

q = Queue()
rq = ReverseQueue()
tsq = TwoStackQueue()

t0 = time.time()
for a in range(99999):
    q.enqueue(a)
t1 = time.time()
total_q = t1-t0
print(total_q, 'queueue')
print(q.size())


t0 = time.time()
for a in range(99999):
    rq.enqueue(a)
t1 = time.time()
total_rq = t1-t0
print(total_rq , 'reverse queueue')
print(rq.size())

t0 = time.time()
for a in range(99999):
    tsq.enqueue(a)
t1 = time.time()
total_rq = t1-t0
print(total_rq , 'two stsack queueue')
print(tsq.inboxSize(), tsq.outboxSize())

t0 = time.time()
while q.is_empty()==False:
    q.dequeue()
t1 = time.time()
total_q = t1-t0
print(total_q)
print(q.size())

t2 = time.time()
while rq.isEmpty() == False:
    rq.dequeue()
t3 = time.time()
total_rq = t3-t2
print(total_rq)
print(rq.size())

t2 = time.time()
while tsq.outboxIsEmpty() == False or tsq.inboxIsEmpty() == False :
    tsq.dequeue()
t3 = time.time()
total_tsq = t3-t2
print(total_tsq)



print(q.size())
print(rq.size())
print(tsq.inboxSize())
print(tsq.outboxSize())