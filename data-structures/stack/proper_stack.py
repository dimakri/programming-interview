class Node:

    def __init__(self, data, nextNode=None):
        self.data = data
        self.nextNode = nextNode


class Stack:

    def __init__(self):
        self.top = None

    def is_empty(self):
        return self.top == None

    def push(self, data):

        node = Node(data=data)

        if self.top is None:
            self.top = node
        else:
            temp = self.top
            self.top = node
            self.top.nextNode = temp

    def pop(self):

        if self.top is None:
            raise ValueError("Stack is empty")

        else:
            self.top = self.top.nextNode

    def peak(self):
        if self.top is None:
            raise ValueError('Stack is empty')
        else:
            return self.top.data.data