from proper_stack import *

class StackNew(Stack):
    
    def min(self):
        
        if self.is_empty is True:
            raise ValueError('Stack is empty!!!')
        else:
            min = self.peak()
            while self.is_empty() is False:
                # print(self.peak())
                if min > self.peak():
                    min = self.peak()
                    self.pop()
                else:
                    self.pop()
            return min

n1 = Node(12)
n2 = Node(3)
n3 = Node(5)
n4 = Node(8)

s = StackNew()
s.push(n1)
s.push(n2)
s.push(n3)
s.push(n4)

print(s.min())

            