arr = []

# def listSum(arr):
#     # standard and straight forward way
#     # of adding all item in a given array
#     sum = 0
#     for i in arr:
#         sum = i + sum
#     return sum


def listSum(arr):
    if len(arr) == 1: # crutial escape clause
        return arr[0]
    elif len(arr) == 0:
        raise StopIteration('Array is empty!')
    else:
        return arr[0] + listSum(arr[1:])


print(listSum(arr))