
__getitem__(key)¶

    Example: fav_color = request.session['fav_color']

__setitem__(key, value)¶

    Example: request.session['fav_color'] = 'blue'

__delitem__(key)¶

    Example: del request.session['fav_color']. This raises KeyError if the given key isn’t already in the session.

__contains__(key)¶

    Example: 'fav_color' in request.session

get(key, default=None)¶

    Example: fav_color = request.session.get('fav_color', 'red')

pop(key, default=__not_given)¶

    Example: fav_color = request.session.pop('fav_color', 'blue')