a = [1,3,5,6,4,23,6]
print(sorted(a))
print(sorted(a, reverse=True))

a = 'PYthon'
print(sorted(a))
print(sorted(a, reverse=True))

a = ('s','v','f','h','h')
print(sorted(a))
print(sorted(a, reverse=True))