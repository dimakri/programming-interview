import asyncio
import time

start = time.time()


def tic():
    return 'at %1.1f seconds' % (time.time() - start)

async def func1():
    print('func1() started work: {}'.format(tic()))
    await asyncio.sleep(2)
    print('func1() stopped work : {}'.format(tic()))

async def func2():
    print('func2() started work: {}'.format(tic()))
    await asyncio.sleep(2)
    print('func2() stopped work : {}'.format(tic()))

async def func3():
    print('Do actual work: {}'.format(tic()))
    await asyncio.sleep(1)
    print('Done : {}'.format(tic()))

ioloop = asyncio.get_event_loop()
task = [ioloop.create_task(func1()), ioloop.create_task(func2()), ioloop.create_task(func3())]
wait_task = asyncio.wait(task)
ioloop.run_until_complete(wait_task)
ioloop.close()
