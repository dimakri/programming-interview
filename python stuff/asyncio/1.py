import asyncio

async def foo():
    print('Running in foo')
    await asyncio.sleep(0)
    print('Explicit context swith to foo() again')

async def bar():
    print('Explicit context to bar()')
    await asyncio.sleep(1)
    print('Implicit context swith back to bar')

ioloop = asyncio.get_event_loop()
task = [ioloop.create_task(foo()), ioloop.create_task(bar())]
wait_task = asyncio.wait(task)
ioloop.run_until_complete(wait_task)
ioloop.close()