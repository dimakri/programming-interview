from collections import Counter

colors = (
    ('Dima', 'Black'),
    ('Oleg', 'Pink'),
    ('Roma', 'Grey'),
    ('Andrey', 'White'),
    ('Eugine', 'White'),
    ('Eugine', 'Green'),
)

favs = Counter(name for name, color in colors)
print(favs)


c = Counter(['a','a','a','b','b','c','c'])
print(c)